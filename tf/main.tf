# TODO: split out to _config.tf?
## _config.tf
terraform {
  required_version = ">= 0.10.1, < 0.12"
}

provider "aws" {
  version = "~> 1.36"
  region = "${var.aws_region}"
}

## end _config.tf

variable "aws_region" {
  default = "us-east-1"
}
variable "ssh_pub_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5BIG3TyFC0gWa4dxR2BfuFdsw8TZzXRjOOXuoRIR2oOOqtDB4/IffHliMg2QE3AR7FdduRkP7amRvWB7xjVBmLDOJBDxycdUOub3pIJW5ha5x2DJPddKBVgxU2TyDkssaGevJ3eWUclZ1EXyY2ORX9GS9rtvTi4yCoJ978zH9DSBy2l2LZnQCS+ViTP/L4IhEHGq3aHIrrd+YsY5r4VEEdI36cFa28Kcafue4fzlHj+m6QlPISjH8oymZy6eKMRUXfQRjDtOzWrs+lQ4kJkHkZu6Z4qDzOT1oQuanwVA5SyWXKWg4xatQMHTaVDuxEYeRzIKe1x3f9whBHy651o6H dhutty@allgoodbits.org"
}

variable "ami" {
  # both from 20180810
  # Amazon Linux 2 HVM 64-bit EBS backend:
  default = "ami-00b94673edfccb7ca"
  # Alternatively: Amazon Linux 2 minimal  HVM 64-bit EBS backend: ami-0f686c64c5fb9828c
}

variable "ssh_user" {
  # For Amazon Linux
  default = "ec2-user"
}

variable "ssh_private_key_path" {
  description = "the path to the private ssh key, perhaps: export TF_VAR_ssh_private_key_path=${HOME}/.ssh/foo.rsa"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "http_port" {
  default = 8080
}

variable "aws_user_prefix" {
  default = "dhutty"
}

variable "default_vpc" {
  description = "the id of the default VPC: perhaps supply with ... -var-file=foo.tfvars or leave as 0 to create a VPC"
  default = "0"
}

variable "product" {
  default = ""
}

variable "squad" {
  default = ""
}

variable "instance_text" {
  default = "$(hostname --fqdn)"
}

output "mvp-ip" {
  value = "${aws_instance.mvp.public_ip}"
}

output "instance_url" {
  value = "http://${aws_instance.mvp.public_ip}:${var.http_port}"
}

output "healthcheck_url" {
  value = "http://${aws_instance.mvp.public_ip}:${var.http_port+1}/healthcheck"
}

resource "aws_security_group" "dh-tf-class-1" {
  name = "dh-tf-class-1"
  #vpc_id = "${var.default_vpc}"
  vpc_id = "${aws_vpc.LISA18.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = "${var.http_port}"
    to_port     = "${var.http_port + 1}"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
 egress {
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.aws_user_prefix}"
    Product = "${var.product}"
    Squad = "${var.squad}"
  }
}

resource "aws_key_pair" "dhutty" {
  key_name   = "dhutty-key"
  public_key = "${var.ssh_pub_key}" ## the only required attribute
}


resource "aws_instance" "mvp" {
  ami = "${var.ami}" ## the only required attribute
  instance_type = "${var.instance_type}"
  key_name      = "${aws_key_pair.dhutty.key_name}"
	# Change the following to "${aws_default_subnet.default_az1.id}" if using a default VPC
	subnet_id = "${aws_subnet.dh-tf-class-1.id }"
  vpc_security_group_ids = ["${aws_security_group.dh-tf-class-1.id}"]
  user_data = <<EOF
#!/bin/bash

echo "${var.instance_text}" > index.html
nohup python -m SimpleHTTPServer "${var.http_port}" &
EOF

  tags {
    Name = "${var.aws_user_prefix}"
    Product = "${var.product}"
    Squad = "${var.squad}"
  }

}

resource "null_resource" "install-python" {
  provisioner "remote-exec" {
    inline = [
      "sudo yum install -y busybox python-virtualenv",
    ]
  }

  connection {
    type        = "ssh"
    private_key = "${file(var.ssh_private_key_path)}"
    user        = "${var.ssh_user}"
    host        =  "${aws_instance.mvp.public_ip}"
  }
  depends_on = ["aws_internet_gateway.igw"]

  provisioner "local-exec" {
    command = "ansible-playbook -vD -i ansible/hosts ansible/playbook.yml"
  }
}

/*
* Create Ansible Inventory File
*
*/

data "template_file" "inventory" {
  template = "${file("${path.module}/templates/inventory.tpl")}"

  vars {
    connection_string_node     = "${format("%s ansible_host=%s", aws_instance.mvp.public_dns, aws_instance.mvp.public_ip)}"
    ssh_user_string             = "${format("ansible_user=%s", var.ssh_user)}"
    ssh_private_key_path_string = "${format("ansible_ssh_private_key_file='%s'", var.ssh_private_key_path)}"
    list_node                   = "${aws_instance.mvp.public_dns}"
  }
}

resource "null_resource" "inventories" {
  provisioner "local-exec" {
    command = "echo '${data.template_file.inventory.rendered}' > ansible/hosts"
  }

  triggers {
    template = "${data.template_file.inventory.rendered}"
  }
}


resource "aws_vpc" "LISA18" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  count = "${var.default_vpc == 0 ? 1 : 0}"
  tags {
    Name = "${var.aws_user_prefix}"
    Product = "${var.product}"
    Squad = "${var.squad}"
  }
}

resource "aws_default_subnet" "default_az1" {
  count = "${var.default_vpc == 0 ? 0 : 1}"
  availability_zone = "us-east-1a"

  tags {
      Name = "Default subnet for us-east-1a"
      Product = "${var.product}"
      Squad = "${var.squad}"
  }
}


resource "aws_network_acl" "vpc_acl" {
  vpc_id = "${var.default_vpc == 0 ? aws_vpc.LISA18.id : var.default_vpc}"
  count = "${var.default_vpc == 0 ? 1 : 0}"

  egress {
    rule_no = 100
    protocol   = "-1"
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    rule_no = 100
    protocol   = "-1"
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  tags {
    Name = "${var.aws_user_prefix}"
    Product = "${var.product}"
    Squad = "${var.squad}"
  }

}

resource "aws_subnet" "dh-tf-class-1" {
  count = "${var.default_vpc == 0 ? 1 : 0}"
  vpc_id = "${var.default_vpc != 0 ? var.default_vpc : aws_vpc.LISA18.id}"
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true

  tags {
    Name = "${var.aws_user_prefix}"
    Product = "${var.product}"
    Squad = "${var.squad}"
  }
}

resource "aws_internet_gateway" "igw" {
  count = "${var.default_vpc == 0 ? 1 : 0}"
  vpc_id = "${var.default_vpc == 0 ? aws_vpc.LISA18.id : var.default_vpc}"
  tags {
    Name = "${var.aws_user_prefix}"
    Product = "${var.product}"
    Squad = "${var.squad}"
  }
}

# DHCP options set
resource "aws_vpc_dhcp_options" "vpc_dhcp" {
  count = "${var.default_vpc == 0 ? 1 : 0}"
  #domain_name         = "${var.aws_cluster_domain}"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags {
    Name = "${var.aws_user_prefix}"
    Product = "${var.product}"
    Squad = "${var.squad}"
  }
}

resource "aws_vpc_dhcp_options_association" "vpc_dhcp_association" {
  vpc_id = "${var.default_vpc == 0 ? aws_vpc.LISA18.id : var.default_vpc}"
  dhcp_options_id = "${aws_vpc_dhcp_options.vpc_dhcp.id}"
  count = "${var.default_vpc == 0 ? 1 : 0}"
}

# Public route table (via IGW)
resource "aws_route_table" "public_rt" {
  vpc_id = "${var.default_vpc == 0 ? aws_vpc.LISA18.id : var.default_vpc}"
  count = "${var.default_vpc == 0 ? 1 : 0}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags {
    Name = "${var.aws_user_prefix}"
    Product = "${var.product}"
    Squad = "${var.squad}"
  }
}

# route for public subnet
resource "aws_route_table_association" "subnet_public_rt_association" {
  count = "${var.default_vpc == 0 ? 1 : 0}"
  subnet_id      = "${aws_subnet.dh-tf-class-1.id}"
  route_table_id = "${aws_route_table.public_rt.id}"
}
